<?php

namespace Scheduler;

/**
 *
 * Class Scheduler
 * @package Scheduler
 */
class Scheduler
{

    const PATTERN_LINE = "{schedule} {task}" . PHP_EOL;

    private $line;

    public function minutely($task)
    {
        $this->custom('* * * * *', $task);

        return $this;
    }

    public function midnight($task)
    {
        $this->custom('@midnight', $task);

        return $this;
    }

    public function rebooting($task)
    {
        $this->custom('@reboot', $task);

        return $this;
    }

    public function hourly($task)
    {
        $this->custom('@hourly', $task);

        return $this;
    }

    public function daily($task)
    {
        $this->custom('@daily', $task);

        return $this;
    }

    public function custom($line, $task)
    {
        $this->line = str_replace(['{schedule}', '{task}'], [$line, $task], self::PATTERN_LINE);

        return $this;
    }

    public function add()
    {
        $jobs = $this->listJobs();
        $this->save($jobs);
    }

    public function removeTask($task, $limit = 0)
    {
        $jobs = $this->listJobs();
        $_jobs = [];
        $index = 0;
        foreach ($jobs as $job) {
            if (stripos($job, $task) === false && (empty($limit) || $index <= $limit)) {
                $_jobs[] = $job;
                $index++;
            }
        }

        return $_jobs;
    }

    public function remove()
    {
        $jobs = $this->listJobs();
        $_jobs = [];
        foreach ($jobs as $job) {
            if ($this->line != $job) {
                $_jobs[] = $job;
            }
        }

        $this->save($_jobs);
    }


    private function save($jobs)
    {
        $dir = sys_get_temp_dir();
        $tmpName = tempnam($dir, 'jobs_');
        file_put_contents($tmpName, implode(PHP_EOL, $jobs));
        $this->runCmd('crontab ' . $tmpName);
        unlink($tmpName);
    }

    private function listJobs()
    {
        $status = 0;
        $out = $this->runCmd('crontab -l');

        if (strpos($out['stderr'], 'no crontab') !== false) {
            $status = 1;
        }

        $jobs = [];
        if ($status == 0) {
            $jobs = explode(PHP_EOL, $out['stdout']);
        }

        return $jobs;
    }

    private function runCmd($cmd)
    {
        $descriptorspec = [
            0 => ["pipe", "r"],  // stdin - канал, из которого дочерний процесс будет читать
            1 => ["pipe", "w"],  // stdout - канал, в который дочерний процесс будет записывать
            2 => ["pipe", "w"] // stderr -
        ];
        $stream = [1 => 'stdout', 'stderr'];
        $proc = proc_open($cmd, $descriptorspec, $pipes);
        $result = [];
        if (is_resource($proc)) {
            foreach ($pipes as $index => $pipe) {
                if ($index == 0) {
                    continue;
                }
                if (is_resource($pipe)) {
                    $result[ $stream[ $index ] ] = trim(stream_get_contents($pipe));
                }
            }
        }

        return $result;
    }
}